<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('cors')->get('/test', function () {
    Log::debug('An informational message.');
    // $users = DB::select('select * from problem_set');
    // foreach ($users as $user) {
    //     echo $user->intro_text;
    // }
    return 'welcome123';
});
