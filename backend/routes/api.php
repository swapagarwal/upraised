<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return 'welcome';
});

Route::middleware('cors')->post('/submit', function (Request $request) {
    // Log::debug('An informational message.');
    // $users = DB::select('select * from problem_set');
    // foreach ($users as $user) {
    //     echo $user->intro_text;
    // }
    // return 'welcome12356';
    $questions = $request->input('questions');
    $answer_set_id = DB::table('answer_set')->insertGetId(
        [
            'problem_set_id' => 1,
            'user_id' => 1
        ]
    );
    foreach ($questions as $question) {
        $answer_id = DB::table('answers')->insertGetId(
            [
                'answer_set_id' => $answer_set_id,
                'question_id' => $question['id'],
                'text' => $question['answer'],
                'rating' => $question['rating'],
                'notes' => $question['notes']
            ]
        );
        // return $answer_id;
    }
    return $questions;
});
